var wot = {
    init: function () {
        $('#startGame').on('click', wot.createGame).addClass('check position');
    },
    createGame: function (json) {
        wot.attempts = 6;
        wot.createWrappers('#wrapper', '<div id="loader" class="hidden" ><img src="img/loading.gif"></div><section id="phraseCont"></section><section id="letterCont"></section><p id="statCont">pozostałych prób:' + wot.attempts + '</p><button class="check" id="checkBtn">Sprawdź</button>');
        wot.fetchJson();
    },
    alphabet: [
        'A',
        'Ą',
        'B',
        'C',
        'Ć',
        'D',
        'E',
        'Ę',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'Ł',
        'M',
        'N',
        'Ń',
        'O',
        'Ó',
        'P',
        'R',
        'S',
        'Ś',
        'T',
        'U',
        'W',
        'Y',
        'Z',
        'Ź',
        'Ż'
    ],
    currentTime: null,
    currentPhrase: null,
    attempts: null,
    setCurrentTime: function(){
        var object = new Date,
                minutes=object.getMinutes(),
                seconds = object.getSeconds(),
                time = minutes+' : '+seconds;
                
        
        console.log(time);
    },
    createWrappers: function (id, content) {
        $(id).html(content);
    },
    createAlphabet: function () {
        var i, length = wot.alphabet.length, content = '';
        for (i = 0; i < length; i++) {
            content += '<button id="' + wot.alphabet[i] + '" class="button">' + wot.alphabet[i] + '</button>';
        }
        $('#letterCont').append(content);
        wot.appendEvent('.button', 'click', wot.checkCorrect);
    },
    appendEvent: function (id, event, name) {
        $(id).each(function () {
            $(this).on(event, name);
        });
    },
    checkCorrect: function (e) {
        wot.checkAttempt();
        $(this).attr('disabled', true);

        var i, length = wot.currentPhrase.length;
        for (i = 0; i < length; i++) {
            if ($(this).attr('id') === wot.currentPhrase[i]) {
                $('input[id="' + i + '"').attr('disabled', true).val(wot.currentPhrase[i]);
            }
        }
    },
    checkGame: function () {
        var result = '', currentPhrase = '';
        $('input').each(function () {
            result += $(this).val();
        });

        currentPhrase = wot.removeSpacesChars();
        if (result.toUpperCase() === currentPhrase) {
            $('#statCont').text('BRAWO! Udało Ci się zgadnąć! :)').addClass('correct');
        } else {
            $('#statCont').text('Nie udało się :( Chcesz zagrać jeszcze raz? Teraz Ci się uda.. :)').addClass('incorrect');
        }
        $('#checkBtn').off('click', wot.checkGame).on('click', wot.createGame).text('Gram dalej!');
        $('#wrapper').append('<button id="negative" class="check">Może później</button>');
        $('#negative').on('click', wot.reload);
        wot.blockEverithing('.phraseLetter', '.button');

    },
    reload: function () {
        location.reload();
    },
    blockEverithing: function (element1, element2) {
        $(element1).attr('disabled', true);
        $(element2).attr('disabled', true);
    },
    removeSpacesChars: function () {
        var length = wot.currentPhrase.length, i, content = '';
        for (i = 0; i < length; i++) {
            if (wot.currentPhrase[i] !== " ") {
                content += wot.currentPhrase[i];
            }
        }
        return content;
    },
    checkAttempt: function () {
        if ((--wot.attempts) === 0) {

            $('button.button').attr('disabled', true);
        }
        $('#statCont').text('pozostałych prób: ' + wot.attempts);
    },
    createInputs: function (currentPhrase) {
        var content = '', length = currentPhrase.length;
        for (var i = 0; i <= length - 1; i++) {
            if (currentPhrase[i] === " ") {
                content += '<br>';
            } else {
                content += '<input id="' + i + '" type="text" maxlength="1" class="phraseLetter"/>';
            }

        }
        $('#phraseCont').append(content);
    },
    fetchJson: function () {
        $.ajax({
            type: 'post',
            url: 'words.json',
            dataType: 'json',
            beforeSend: function () {
                $('#loader').removeClass('hidden');
            }
        }).always(function () {
            $('#loader').addClass('hidden');
        }).done(function (json) {
            wot.setCurrentTime();
            wot.createAlphabet();
            wot.randomPhrase(json);
            wot.appendEvent('#checkBtn', 'click', wot.checkGame);

        }).fail(function () {
            alert('przepraszam, nie znaleziono wyrazu :(');
        });
    },
    randomPhrase: function (json) {
        var length = json.length - 1, index;
        index = json[Math.round((Math.random() * length))];
        while (index === wot.currentPhrase) {
            console.log(index);
            index = json[Math.round((Math.random() * length))];
        }
        wot.currentPhrase = index;
        console.log(wot.currentPhrase);
        wot.createInputs(wot.currentPhrase);
    }
};
$(document).ready(function () {
    wot.init();
});